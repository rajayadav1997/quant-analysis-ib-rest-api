import traceback

from config import logger,db_obj


class UserState(db_obj.get("obj").Model):
    """
    This module store all info about users' states eg. auth, payment, etc..
    """
    id = db_obj.get("obj").Column(db_obj.get("obj").Integer, primary_key=True, autoincrement=True)
    user_id = db_obj.get("obj").Column(db_obj.get("obj").String, nullable=False)
    password = db_obj.get("obj").Column(db_obj.get("obj").String, nullable=False)
