
from flask import Flask, request
from flask import Flask, request,render_template,redirect,jsonify,url_for,session
import json
import time
import sys
import sqlalchemy.sql.default_comparator
import threading
from mail_util import *
from config import session_key,flask_host,flask_port,logger,db_obj,timeframe,ib_obj
import pandas as pd
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__,  static_url_path='/static')


from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database

db_name = "quant_analysis"
try:
    if (sys.argv[1] != None) and (sys.argv[1] != ""):
        print(sys.argv[1])
        db_name = sys.argv[1]
except Exception as e:
    print("argument not found")

db_url = "postgresql://postgres:postgres@localhost:5432/"+db_name
engine = create_engine(db_url)
if not database_exists(engine.url):
    create_database(engine.url)

app.config['SQLALCHEMY_DATABASE_URI'] =  db_url
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db_obj.update({"obj":SQLAlchemy(app)})
from db_models import user_state
from db_models import candel_difference
from db_models import candel_data
db_obj.get("obj").create_all()

from apiConnection import *
from twsConnection import *

# CORS(app)
import random

random_key = random.randint(0,22)


@app.route("/",methods=['GET'])
def login():
    return render_template('Login.html')

@app.route("/login",methods=['POST'])
def submit_form():
    try:
        global random_key
        usernmae = request.form['username']
        password = request.form['password']
        data = user_state.UserState.query.filter(user_state.UserState.user_id == usernmae).filter(user_state.UserState.password == password).first()
        if data == None:
            return "Unauthorized User",200
        else:
            random_key = random.randint(0, 22)
            print(random_key)
            session['response'] = random_key
            return redirect(url_for("home"))
    except Exception as e:
        traceback.print_exc()
        return "Unauthorized User",200

@app.route("/home",methods=['GET'])
def home():
    global random_key
    print(random_key)
    if 'response' in session and session['response'] == random_key:
        return render_template('Home.html')
    else:
        return redirect(("/"))



@app.route("/CandelData",methods=['GET'])
def get_candel_data():
    date_check={}
    entity = candel_data.CandelData.query.all()
    response={}
    for data in entity:
        if response.get(data.base_symbol) == None:
            response.update({data.base_symbol:[]})
        if data.sma200 != 0:
            if (date_check.get(data.base_symbol) == None):
                date_check.update({data.base_symbol:data.candel_time.date()})
            else:
                if(date_check.get(data.base_symbol) != data.candel_time.date() ):
                    sym_data = response.get(data.base_symbol)
                    sym_data = []
                    response.update({data.base_symbol: []})
                    date_check.update({data.base_symbol: data.candel_time.date()})
            sym_data = response.get(data.base_symbol)
            sub_res = {}
            sub_res.update({"candel_time": str(data.candel_time) })
            sub_res.update({"o": data.o})
            sub_res.update({"h": data.h})
            sub_res.update({"l": data.l})
            sub_res.update({"c": data.c})
            sub_res.update({"v": data.v})
            sub_res.update({"sma50": data.sma50})
            sub_res.update({"sma200": data.sma200})
            sym_data.append(sub_res)

    return json.dumps(response),200

class Main():
    def __init__(self):
        app.secret_key = session_key

#  calculate sleep time
def getTimeAccordingFrame():
    try:
        now = datetime.datetime.now()
        midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
        seconds = (now - midnight).seconds
        currtimep = seconds
        sec = currtimep + (timeframe - (currtimep % timeframe))
        sleepSec = ((timeframe - (currtimep % timeframe)))

        logger.info(f" now was - {now}  , we are sleeping -   {sleepSec} s")
        return sleepSec
    except Exception as e:
        print(e)
        return 2


def remove_async_thread(key,time_obj,obj):
    newtime = time_obj + datetime.timedelta(seconds=15)
    logger.info(f"time check running {key} old time {time_obj} , new time {newtime} is done {obj.done()}")
    asyncio.set_event_loop(ib_obj.get("tws").event)
    nest_asyncio.apply()
    while True:
        try:
            time.sleep(1)
            if(datetime.datetime.now() > newtime):
                if obj.done() == False:
                    logger.info(f"we are cancelling async obj because it is halting.... {key} {time_obj}")
                    obj.cancel()
                else:
                    logger.info(f"request successfully done {key} old time {time_obj} , new time {newtime} is done {obj.done()}")
                break

        except Exception as e:
            logger.error(f"error in removing async thread {e}")

def run_candel_thread():
    asyncio.set_event_loop(ib_obj.get("tws").event)
    nest_asyncio.apply()
    while True:
        try:
            data = open("symbol.json", 'r')
            default_symbol = data.read()
            default_symbol = json.loads(default_symbol)
            data.close()
            time.sleep(getTimeAccordingFrame())
            for key in default_symbol.keys():
                maturityDate = default_symbol.get(key).get("maturityDate")
                exchange = default_symbol.get(key).get("exchange")
                #

                x = asyncio.ensure_future(get_market_history_data(key,maturityDate,exchange))
                threading.Thread(target=remove_async_thread , args=(key,datetime.datetime.now(),x,)).start()


        except Exception as e:
            logger.error(f"error in running run_candel_thread {e}")


if __name__ == "__main__":
    Main()
    connect_email()
    # threading.Thread(target=ping_server_for_live).start()
    # getting_con_id()
    # subscribe_market()

    global ib_obj
    logger.info("app starting....")
    ib_obj.update({"tws":twsConnection()})
    logger.info("Connection object created")
    ib_obj.get("tws").connectapi()
    logger.info("app starting....")

    threading.Thread(target=run_candel_thread).start()
    app.run(host=flask_host, port=flask_port, debug=False)


# def getting_con_id():
#     for key in default_symbol.keys():
#         try:
#             conid = get_contract(key)
#             if conid != None:
#                 val = default_symbol.get(key)
#                 val.update({"conid": conid})
#         except Exception as e:
#             logger.info(f"error in diff thread {e}")

# def ping_server_for_live():
#     try:
#         while True:
#             time.sleep(5)
#             ping_session()
#     except Exception as e:
#         logger.error(f"error in ping {e}")


# def subscribe_market():
#     try:
#         verify_accounts()
#         cancel_all_sub_market()
#         conids=[]
#         for key in default_symbol.keys():
#                 conid = default_symbol.get(key).get("conid")
#                 if conid != None:
#                     conids.append(conid)
#         sub_market(conids)
#     except Exception as e:
#         logger.info(f"error in subscribe data  {e}")


#
# def run_candel_thread():
#     while True:
#         try:
#             time.sleep(getTimeAccordingFrame())
#             for key in default_symbol.keys():
#                 conid = default_symbol.get(key).get("conid")
#                 if conid != None:
#                     get_market_history_data(conid, default_period, default_bar)
#         except Exception as e:
#             logger.error(f"error in running run_candel_thread {e}")