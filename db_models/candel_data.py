import traceback

from config import logger,db_obj


class CandelData(db_obj.get("obj").Model):
    """
    This module store all info about users' states eg. auth, payment, etc..
    """
    id = db_obj.get("obj").Column(db_obj.get("obj").Integer, primary_key=True, autoincrement=True)
    o = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    h = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    l = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    c = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    v = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    sma50 = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    sma200 = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    symbol = db_obj.get("obj").Column(db_obj.get("obj").String, nullable=False)
    base_symbol = db_obj.get("obj").Column(db_obj.get("obj").String)
    candel_time = db_obj.get("obj").Column(db_obj.get("obj").DateTime, nullable=False)

    def __str__(self):
        return {"id":self.id}

    def toString(self):
        return {"id":self.id,"open":self.o,"high":self.h,"low":self.l,"close":self.c,"v":self.v,"date":self.candel_time,"sma50":self.sma50,"sma200":self.sma200}