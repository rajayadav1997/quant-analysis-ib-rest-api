import traceback

from config import logger,db_obj


class CandelDifference(db_obj.get("obj").Model):
    """
    This module store all info about users' states eg. auth, payment, etc..
    """
    id = db_obj.get("obj").Column(db_obj.get("obj").Integer, primary_key=True, autoincrement=True)
    o = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    h = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    l = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    c = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    v = db_obj.get("obj").Column(db_obj.get("obj").Float, nullable=False)
    candel_time = db_obj.get("obj").Column(db_obj.get("obj").String, nullable=False)
