from config import *
import nest_asyncio
import asyncio
import threading
import datetime
from ib_insync import IB,util,Forex,Stock,Order,StopOrder,StopLimitOrder,Trade,Fill,Future,CFD,Option,Position,LimitOrder
import time
import traceback
ib_event = None
class twsConnection():
    def __init__(self):
        self.host=tws_host
        self.port = tws_port
        self.timeout = 2000
        self.ib_obj=None
        self.event = None

    def createContract(self,inst_type=None,symbol=None,lastTradeDateOrContractMonth=None,exchange=None,currency=None,strike=None,right=None,base_symbol=None,maturityDate=None):
        if inst_type.upper() == 'STK':
            contractDetails = Stock(symbol.upper(), exchange.upper(), currency.upper())
        elif inst_type.upper() == 'CASH':
            contractDetails = Forex(symbol.upper())
        elif inst_type.upper() == 'CFD':
            contractDetails = CFD(symbol.upper())
        elif inst_type.upper() == 'FUT':
            contractDetails = Future(exchange=exchange.upper(), symbol=symbol.upper(), lastTradeDateOrContractMonth=maturityDate)
            # if symbol== "NQ":
            #     contractDetails = Future(exchange=exchange.upper(), symbol=symbol.upper(), lastTradeDateOrContractMonth='20210917')
            # elif symbol=="ES":
            #     contractDetails = Future(exchange=exchange.upper(), symbol=symbol.upper(), lastTradeDateOrContractMonth='20210917')
            # else:
            #     contractDetails = Future(exchange=exchange.upper(), symbol=symbol.upper(), lastTradeDateOrContractMonth='20210917')
        elif inst_type.upper() == 'OPT':
            # localSymbol = "C AAPL  NOV 20  112.5"
            contractDetails = Option(exchange=exchange.upper(),lastTradeDateOrContractMonth=lastTradeDateOrContractMonth,strike=strike,right=right.upper(), symbol=symbol.upper(), currency=currency.upper())
            # contractDetails = Option(exchange='SMART', lastTradeDateOrContractMonth='20201105', strike="112.5", right="C", symbol='AAPL', currency='USD')
        return contractDetails

    def get_req_contract(self,contractDetails):
        asyncio.set_event_loop(ib_event)
        nest_asyncio.apply()
        return self.ib_obj.reqContractDetails(contractDetails)

    def createOrder(self,trade_type=None,action=None,quantity=None,stop_price=None):
        order = None
        if (trade_type.upper() == 'SNAPMKT'):
            order = Order(orderType='SNAP MKT', action=action.upper(), totalQuantity=quantity, auxPrice=stop_price)
        elif (trade_type.upper() == 'PEGMKT'):
            order = Order(orderType='PEG MKT', action=action.upper(), totalQuantity=quantity, auxPrice=stop_price)
        elif (trade_type.upper() == 'MKT'):
            order = Order(orderType='MKT', action=action.upper(), totalQuantity=quantity)
        elif (trade_type.upper() == 'MOC'):
            order = Order(orderType='MOC', action=action.upper(), totalQuantity=quantity)
        return order


    def get_candel(self, ibcontract,what_to_show,symbol=""):

        asyncio.set_event_loop(self.event)
        nest_asyncio.apply()
        for x in range(0, 3):
            try:
                    logger.info(f'getting histoprical data {what_to_show} {symbol}')
                    histData = self.ib_obj.reqHistoricalData(contract=ibcontract, endDateTime='', formatDate=1, whatToShow=what_to_show, durationStr="1 D", barSizeSetting='1 min',
                                                    useRTH=useRTH)
                    # self.ib_obj.waitOnUpdate()
                    logger.info(f'{symbol} historical dtaa {histData}')
                    if len(histData) == 0 or histData == None:
                        logger.info(f"retrying candel data , price is not comming,")
                        time.sleep(1)
                    else:
                        return histData[0:len(histData)-1]
            except Exception as e:
                traceback.print_exc()
                logger.error(f"error in geting historical data {e}")
                time.sleep(1)
            return None

        # if histData[-1].ask != None and histData[-1].ask != 0:
        #     return histData[-1].ask
        # else:
        #     return histData[-1].close

    def place_order(self,ib_contract,trade_order,account=""):
        asyncio.set_event_loop(self.event)
        nest_asyncio.apply()
        logger.error(f"we are placing order {ib_contract} {trade_order}")
        if account != "":
            trade_order.account = account
        return self.ib_obj.placeOrder(contract=ib_contract, order=trade_order)

    def cancel_trade(self,order):
        try:
            asyncio.set_event_loop(self.event)
            nest_asyncio.apply()
            self.ib_obj.cancelOrder(order)
        except Exception as e:
            logger.error(f"error in cancelling order {e}")
    def position_check(self):
        return self.ib_obj.positions()

    def position_event(self,position:Position):
        print(f"position callback  {position}")



    def tickOptionComputation(self, reqId, tickType, tickAttrib: int,impliedVol: float, delta: float, optPrice: float, pvDividend: float,gamma: float,
                              vega: float, theta: float, undPrice: float):
        logger.info("TickOptionComputation. TickerId:", reqId, "TickType:", tickType,"TickAttrib:", tickAttrib,"ImpliedVolatility:", impliedVol, "Delta:", delta, "OptionPrice:",optPrice, "pvDividend:", pvDividend, "Gamma: ", gamma, "Vega:", vega,"Theta:", theta, "UnderlyingPrice:", undPrice)

    def callback_run(self,ib, event):
        asyncio.set_event_loop(event)
        while True:
            try:
                ib.sleep(1)
            except Exception as e:
                print("error in call back")

    def qualify_contract(self,ibcontract):
        self.ib_obj.qualifyContracts(ibcontract)

    def req_mkt_DataType(self):
        # nest_asyncio.apply()
        return self.ib_obj.reqMarketDataType(1)

    def subscribe_market_depth(self, currencyPair):
        try:
            logger.info("requesting market depth")
            asyncio.set_event_loop(self.event)
            nest_asyncio.apply()
            self.ib_obj.qualifyContracts(currencyPair)
            res = self.ib_obj.reqMktDepth(currencyPair, numRows=20)
            logger.info(f"depth data {res}")
            res.updateEvent += self.onTickerUpdate
            ib_obj.sleep(1)
            return res
        except Exception as e:
            traceback.print_exc()
            logger.error('error in req market depth ' + str(e))


    def req_mkt_Data(self,ibcontract):
        asyncio.set_event_loop(ib_event)
        nest_asyncio.apply()
        res = self.ib_obj.reqMktData(contract=ibcontract,genericTickList="",snapshot=False,regulatorySnapshot=False,mktDataOptions=[])
        res.updateEvent += self.onTickerUpdate
        self.ib_obj.sleep(1)
        return res

    def getTickByTick(self,ibcontract):
        try:
            tickers = self.ib_obj.ticker(ibcontract)
            logger.info(f"Ticker Found {str(tickers)} for {ibcontract}"  )
            return tickers
        except Exception as e:
            traceback.print_exc()
            logger.error('req tick data ' + str(e))
    def cancelTickData(self, currencyPair):
        try:
            asyncio.set_event_loop(self.event)
            nest_asyncio.apply()
            execution = self.ib_obj.cancelMktData(contract=currencyPair)
            return execution
        except Exception as e:
            logger.error('cancel market data ' + str(e))
            return None

    def reconnected(self):
        while True:
            try:
                asyncio.set_event_loop(self.event)
                if self.ib_obj.isConnected() == False:
                    logger.info("we are reconnecting.....")
                    try:
                        self.ib_obj.disconnect()
                    except Exception as e:
                        logger.error(f"error in disconnecting {e}")
                    self.connectapi()
                time.sleep(60)
            except Exception as e:
                logger.error(f"error in reconnecting {e}")

    def connectapi(self):
        try:
            print('first connect')
            # nest_asyncio.apply()
            global ib_event
            self.ib_obj = IB()
            # self.ib_obj.MaxSyncedSubAccounts = 1
            ib_event = asyncio.get_event_loop()

            logger.info("IB CONNECTING - Event loop requested")
            asyncio.sleep(3)
            self.ib_obj.connect(self.host, self.port, timeout=self.timeout)
            logger.info("Ib connect called ")
            asyncio.sleep(3)
            if self.ib_obj.isConnected() != False:
                logger.info("Calling ib.connect()")
                self.event = asyncio.get_event_loop()

            self.ib_obj.orderStatusEvent += self.orderStatusloop
            logger.info("Order status subscribed ")
            self.ib_obj.disconnectedEvent += self.disconnectfunc
            logger.info("disconnectedEvent subscribed ")
            asyncio.sleep(3)
            self.ib_obj.errorEvent += self.errorfunc
            logger.info("errorEvent subscribed ")
            self.ib_obj.positionEvent += self.position_event
            logger.info("positionEvent subscribed ")
            asyncio.sleep(3)
            logger.info('Call Back thread starting ')
            th = threading.Thread(target=self.callback_run, args=(self.ib_obj, self.event))
            th.start()
            threading.Thread(target=self.reconnected).start()
            logger.info('Call Back thread started ')
            self.req_mkt_DataType()
            ## Ib need to be sllep for 1 second otherwise it don't work
            logger.info('Successfully connected to the API')
        except Exception as e:
            traceback.print_exc()
            logger.error(f"error in connecting function {e}")
    def onTickerUpdate(self, ticker=None):
        try:
            if (ticker != None):
                userSymbol = ticker.contract.symbol
                if ticker.contract.secType.upper() == "CASH":
                    userSymbol = ticker.contract.symbol + ticker.contract.currency
                #     secType = ticker.contract.secType
                #     currency = ticker.contract.currency
                #     symbol = ticker.contract.symbol
                #     contract = ticker.contract
                #     ticker.domBids, ticker.domAsks
        except Exception as e:
            traceback.print_exc()
            logger.error(f"error in main logic {e} {ticker}")


    def orderStatusloop(self,trade: Trade):
        try:
            logger.info(f"order status callback {trade}")
            # if (trade.orderStatus.status == 'Filled'):
            #     if (buy_order_dict.get(trade.order.orderId) != None) and (buy_order_dict.get(trade.order.orderId) == True):

        except Exception as e:
            logger.error(f"error in status loop {e} {trade}")


            # if(trade.orderStatus.status == 'PendingSubmit')
            # elif(trade.orderStatus.status == 'PendingCancel')
            # elif(trade.orderStatus.status == 'PreSubmitted')
            # elif(trade.orderStatus.status == 'Submitted')
            # elif(trade.orderStatus.status == 'Cancelled')
            # elif(trade.orderStatus.status == 'Inactive')
            # elif(trade.orderStatus.status == 'PartiallyFilled')
            # elif(trade.orderStatus.status == 'Rejected')

    def errorfunc(self,reqId,errorCode,errorString,contract):
        logger.info(f"Error function call {reqId} {errorCode} {errorString} {contract} ")

    def get_open_Trades(self,user_symbol = None, account=None):
        logger.info('getting open trade')
        try:
            open_trade = 0
            old_order= None
            orders = self.ib_obj.openTrades()
            for order in orders:
                if(order.contract.symbol == user_symbol) and ((account == '0') or (account == order.order.account)):
                    open_trade = open_trade + 1
                    old_order = order
                    break
            return open_trade,old_order
        except Exception as e:
            logger.error(f'error in closing open orders {e}')
        return 0

    def disconnectfunc(self):

        logger.info('ib Disconnected... trying to connect')
        # while True:
        #     if not (self.ib_obj.isConnected()):
        #         try:
        #             logger.info('ib Disconnected... trying to connect')
        #             self.connectapi()
        #             # config.api_conn = api_conn
        #         except Exception as e:
        #             print(e)
        #         logger.info(' ib not connected')
        #     else:
        #         logger.info(' ib connected')
        #         break
        #     time.sleep(20)
