from config import  logger,db_obj,ib_obj
import requests
import time
import datetime
import pandas as pd
import threading
import math
import asyncio
import nest_asyncio
from mail_util import *
import traceback
from db_models import candel_data,user_state
headers = {"Content-Type": "application/json"}

# def ping_session():
#     try:
#         res = requests.get('https://localhost:5000/v1/api/tickle', headers=headers,verify=False)
#         logger.info(f"ping response =  {res.text}")
#     except Exception as e:
#         logger.error(f"error in ping {e}")
#
#
# def get_contract(symbol):
#     try:
#         for x in range(0,3):
#             logger.info(f"we are getting contract {symbol}")
#             jsonData = {
#                 "symbol": symbol,
#                 "name": True,
#                 "secType": "FUT"
#                 }
#
#             res = requests.post('https://localhost:5000/v1/api/iserver/secdef/search',json=jsonData, headers=headers,verify=False)
#             logger.info(f" contract {symbol}   =  {res.text}")
#             if res.status_code == 200:
#                 return res.json()[0]['conid']
#             else:
#                 logger.error(f"error in getting con id status {res.status_code}")
#                 time.sleep(1)
#     except Exception as e:
#         logger.error(f"error in getting contract {e}")
#     return None
#
# def verify_accounts():
#     try:
#         while True:
#             logger.info("we are verifying account")
#             res = requests.get('https://localhost:5000/v1/api/iserver/accounts', headers=headers,verify=False)
#             logger.info(f" verifying account res  =  {res.text}")
#             if res.text == "{}":
#                 logger.info(f"accounts not found we cannot run this application, check your username and password.")
#             else:
#                 break
#             time.sleep(1)
#     except Exception as e:
#         logger.error(f"error in  verifying accounts {e}")
#
# def cancel_all_sub_market():
#     try:
#         logger.info("we are cancelling market data")
#         res = requests.get('https://localhost:5000/v1/api/iserver/marketdata/unsubscribeall', headers=headers,verify=False)
#         logger.info(f"cancelling market data  =  {res.text}")
#     except Exception as e:
#         logger.error(f"error in cancelling data {e}")
#
# def sub_market(conids):
#     try:
#         for x in range(0,3):
#             logger.info("we are subscribing market data")
#             res = requests.get(f'https://localhost:5000/v1/api/iserver/marketdata/snapshot?conids={conids}', headers=headers,verify=False)
#             logger.info(f"sub market data {conids}   =  {res.text}")
#             if res.status_code == 200:
#                 break
#     except Exception as e:
#         logger.error(f"error in subscribing data {e}")
#
# def cancel_market():
#     try:
#         logger.info("we are cancelling market data")
#         res = requests.get('https://localhost:5000/v1/api/iserver/marketdata/unsubscribeall', headers=headers,verify=False)
#         logger.info(f"cancel market data   =  {res.text}")
#     except Exception as e:
#         logger.error(f"error in cancelling data {e}")




async def get_market_history_data(symbol,maturityDate,exchange):
    try:
        await asyncio.sleep(0)
        for x in range(0,2):
            logger.info("we are getting history data")
            ibcontract = ib_obj.get("tws").createContract(inst_type="FUT",symbol=symbol,lastTradeDateOrContractMonth=None,exchange=exchange,currency=None,strike=None,right=None,base_symbol=None,maturityDate=maturityDate)
            res = ib_obj.get("tws").get_candel(ibcontract,"TRADES",symbol)
            if res  != None:
                save_candel_in_db(symbol,res,(ibcontract.symbol + str(ibcontract.lastTradeDateOrContractMonth)))
                check_difference(symbol,res,(ibcontract.symbol + str(ibcontract.lastTradeDateOrContractMonth)))
                break
            time.sleep(1)
    except Exception as e:
        traceback.print_exc()
        logger.error(f"error in getting history data {e}")
def save_candel_in_db(symbol,candel,base_symbol=""):
    try:
        logger.info(f"saving data {candel}")
        data = []
        for val in candel:
            data.append({"date":val.date,"open":val.open,"high":val.high,"low":val.low,"volumn":val.volume,"close":val.close})


        df = pd.DataFrame(data)
        df['sma50'] = df.iloc[:, 1].rolling(window=50).mean()
        df['sma200'] = df.iloc[:, 1].rolling(window=200).mean()
        for ind in df.index:
            settings = candel_data.CandelData.query.filter(candel_data.CandelData.candel_time == (df['date'][ind])).filter(candel_data.CandelData.base_symbol == base_symbol).first()
            if settings == None:
                candel_db = candel_data.CandelData()
                candel_db.o = float(df['open'][ind])
                candel_db.h = float(df['high'][ind])
                candel_db.l =  float(df['low'][ind])
                candel_db.v = float(df['volumn'][ind])
                candel_db.c = float(df['close'][ind])
                candel_db.base_symbol = base_symbol
                candel_db.symbol = symbol
                candel_db.sma50 = 0 if math.isnan(df['sma50'][ind]) else float(df['sma50'][ind])
                candel_db.sma200 = 0 if math.isnan(df['sma200'][ind]) else float(df['sma200'][ind])
                candel_db.candel_time = df['date'][ind]
                db_obj.get("obj").session.add(candel_db)
                db_obj.get("obj").session.commit()


    except Exception as e:
        traceback.print_exc()
        logger.error(f"error in saving candel {e}")

# symbol and data
def check_difference(symbol,data,base_symbol=""):
    try:
        prev_data = candel_data.CandelData.query.filter(candel_data.CandelData.base_symbol == base_symbol).all()
        threading.Thread(target=run_diff_check,args=(data,base_symbol,prev_data,)).start()
    except Exception as e:
        logger.error(f"error in checking difference {e}")


def run_diff_check(current_data,symbol,prev_data):
    asyncio.set_event_loop(ib_obj.get("tws").event)
    nest_asyncio.apply()
    try:
        if prev_data != None and len(prev_data) > 0:
            for prev_index in prev_data:
                for curr_index in current_data:
                        if (curr_index.date == prev_index.candel_time) and ((curr_index.high != prev_index.h) or (curr_index.open != prev_index.o) or (curr_index.low != prev_index.l) or curr_index.close != prev_index.c):
                            logger.info(f"{symbol} data is different at index  prev data {prev_index.toString()} current data {curr_index}")
                            send_email(f"Symbol - {symbol} ->->-> \n\n  Previous Candel Data [ DateTime - {prev_index.candel_time} , Open - {prev_index.o} , High - {prev_index.h} , Low - {prev_index.l} , Close - {prev_index.c} ] \n\n"
                                       f" Current Candel Data [  DateTime - {curr_index.date} , Open - {curr_index.open} , High - {curr_index.high} , Low - {curr_index.low} , Close - {curr_index.close} ] ", mail_obj.get("receiver_mail"), subject="Bot Msg", username="")
    except Exception as e:
        traceback.print_exc()
        logger.error(f"error in checking diff main logic {symbol} {e} ")




# def get_market_history_data(conid,period,bar):
#     try:
#         for x in range(0,2):
#             logger.info("we are getting history data")
#             res = requests.get('https://localhost:5000/v1/api/iserver/marketdata/history?conid=' + str(conid)+'&period='+period+'&bar='+bar, headers=headers,verify=False)
#             logger.info(f"history market data {conid}   =  {res.text}")
#             if res.status_code == 200:
#                 data = res.json()
#                 save_candel_in_db(data)
#                 check_difference(data['symbol'],data)
#                 break
#             time.sleep(1)
#     except Exception as e:
#         logger.error(f"error in getting history data {e}")
# def save_candel_in_db(data):
#     try:
#         logger.info(f"saving data {data['symbol']}")
#         candel = data['data']
#         working_index = None
#         for index in range(len(candel) -1,-1,-1):
#
#             t = datetime.datetime.fromtimestamp(int(candel[index]['t']) / 1000).strftime('%Y-%m-%d %H:%M:%S')
#             settings = candel_data.CandelData.query.filter(candel_data.CandelData.candel_time == (t)).first()
#             if settings != None:
#                 break
#             else:
#                 working_index = index
#         df = pd.DataFrame(candel)
#         df['sma50'] = df.iloc[:, 1].rolling(window=50).mean()
#         df['sma200'] = df.iloc[:, 1].rolling(window=200).mean()
#
#         if working_index != None:
#             for ind in df.index:
#                 candel_db = candel_data.CandelData()
#                 candel_db.o = float(df['o'][ind])
#                 candel_db.h = float(df['h'][ind])
#                 candel_db.l =  float(df['l'][ind])
#                 candel_db.v = float(df['v'][ind])
#                 candel_db.c = float(df['c'][ind])
#                 candel_db.symbol = data['symbol']
#                 candel_db.sma50 = 0 if math.isnan(df['sma50'][ind]) else float(df['sma50'][ind])
#                 candel_db.sma200 = 0 if math.isnan(df['sma200'][ind]) else float(df['sma200'][ind])
#                 candel_time = str(df['t'][ind])
#                 candel_db.candel_time = datetime.datetime.fromtimestamp(int(candel_time) / 1000).strftime('%Y-%m-%d %H:%M:%S')
#                 db_obj.get("obj").session.add(candel_db)
#                 db_obj.get("obj").session.commit()
#
#         else:
#             logger.info(f"{data['symbol']} working index {working_index} , len in data {len(candel)}")
#
#     except Exception as e:
#         traceback.print_exc()
#         logger.error(f"error in saving candel {e}")
#
# # symbol and data
# prev_candel_data={}
# def check_difference(symbol,data):
#     try:
#         if prev_candel_data.get(symbol) == None:
#             logger.info(f"first data so we cannot find difference {symbol}")
#         else:
#             logger.info(f"two data found, now we are canculating diff")
#             threading.Thread(target=run_diff_check,args=(prev_candel_data.get(symbol)['data'],data['data'],symbol,)).start()
#         prev_candel_data.update({symbol:data})
#     except Exception as e:
#         logger.error(f"error in checking difference {e}")
#
#
# def run_diff_check(prev_data,current_data,symbol):
#     try:
#         msg = []
#         for prev_index in range(0,len(prev_data)):
#             for curr_index in range(0,len(current_data)):
#                 if current_data[curr_index] != prev_data[prev_index]:
#                     if current_data[curr_index]['t'] == prev_data[prev_index]['t']:
#                         send_email(f"{symbol} data is different at index  prev data {prev_data[prev_index]} current data {current_data[curr_index]}", "apachiheart@gmail.com", subject="Bot Msg", username="")
#     except Exception as e:
#         traceback.print_exc()
#         logger.error(f"error in checking diff main logic {symbol} {e} ")
