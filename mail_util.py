import smtplib, ssl
from config import mail_obj,logger
import json
smtp_server = mail_obj.get("smtp_server")
port = mail_obj.get("port")
sender_email = mail_obj.get("sender_email")
password = mail_obj.get("password")

# Create a secure SSL context
context = ssl.create_default_context()
server = None

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def connect_email():
    try:
        logger.info('connecting mail')
        server = smtplib.SMTP(smtp_server,port)
        server.ehlo() # Can be omitted
        server.starttls(context=context) # Secure the connection
        server.ehlo() # Can be omitted
        server.login(sender_email, password)
        mail_obj.update({"obj":server})
        return True
    except Exception as e:
        # Print any error messages to stdout
        logger.error(f"error in connecting mail {e}")
        return False

def send_email(data,receiver,subject="HFT Bot",username="Alpha"):
    try:
        if mail_obj.get("obj") == None or mail_obj.get("obj").noop()[0] != 250:
            logger.info("mail server disconnect we are connecting")
            connect_email()
        else:
            logger.info(f'mail server already connected {mail_obj.get("obj").noop()[0]}')
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = sender_email
        msg['To'] = receiver

        # part1 = MIMEText(json.dumps(data), 'text')
        # msg.attach(part1)
        message = 'Subject: {}\n\n{}'.format(subject, data)
        mail_obj.get("obj").sendmail(sender_email, receiver, message)
    except Exception as e:
        logger.error(f"error in sending  mail error msg [ {e} ] {subject} to {receiver}")

        # if mail_server_object.get("data") == None or mail_server_object.get("data").noop()[0] != 250:
        #     logging.info("mail server disconnect we are connecting")
        #     connect_email()
        # else:
        #     logging.info(f'mail server already connected {mail_server_object.get("data").noop()[0]}')
        #
        # msg = MIMEMultipart('alternative')
        # msg['Subject'] = subject
        # msg['From'] = sender_email
        # msg['To'] = receiver
        # # Create the body of the message (a plain-text and an HTML version).
        # # text = "Hi!\nHow are you?\nHere is the link you wanted:\nhttp://www.python.org"
        # html = None
        # if side == "BUY":
        #     html = buy_email_html.return_msg(symbolq, oldcolor, newcolor, oldvalue, newValue, username)
        # else:
        #     html = sell_email_html.return_msg(symbolq, oldcolor, newcolor, oldvalue, newValue, username)
        # # Record the MIME types of both parts - text/plain and text/html.
        # # part1 = MIMEText(text, 'plain')
        # part1 = MIMEText(html, 'html')
        # msg.attach(part1)
        # mail_server_object.get("obj").sendmail(sender_email, receiver, msg.as_string())
