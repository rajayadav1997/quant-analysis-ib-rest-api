import sys
import os
from cx_Freeze import setup, Executable
from datetime import datetime
import requests



build_exe_options = {
	"packages": [
		"flask",
		"flask_sqlalchemy",
		"flask_cors",
		"asyncio",
		"encodings",
		"jinja2",
		"pandas",
		"plyer"

	],
	"excludes": [
		'tkinter'
	],
	"include_files": [
		os.path.join(os.path.dirname(os.path.abspath(__file__)), 'DLLs', 'sqlite3.dll'),
		os.path.join(os.path.dirname(os.path.abspath(__file__)), 'DLLs', 'vcredist_x64.exe'),
		"static",
		"templates",
		"db_models"
		# TODO: recursively include files in html directory
	],
	"include_msvcr": True
}

setup(
	name="Get Software",
	version="1.0",
	description="This is a server for the TradingView Chrome Plugin",
	options={"build_exe": build_exe_options},
	executables=[Executable("app.py", base=None)]
)