import logging
from logging.config import dictConfig
flask_host = "0.0.0.0"
flask_port = "80"

db_obj ={"url":"postgresql://postgres:postgres@localhost:5432/quant_analysis",
"obj":None
}

mail_obj={ "smtp_server" :"smtp.gmail.com","port" : 587,
           "sender_email": "bestmovieclip1997@gmail.com",
           "password" : "bestmovieclip", "obj":None,"receiver_mail":"apachiheart@gmail.com"}

session_key="&###@#gdgdg&*%$d"
prod = True
logging_type = "DEBUG"
LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(threadName)s - %(thread)d - %(asctime)s  - %(name)s - %(funcName)s - %(lineno)d - %(levelname)s - %(message)s'
        }
    },
    'handlers': {
        'debugfilehandler': {
            'level': logging_type,
            'class': 'logging.FileHandler',
            'filename': 'app.log',
            'formatter': 'default'
        },
        'rotatingfile': {
            'maxBytes': 5 * 1024 * 1024 * 90,
            'backupCount': 30,
            'level': logging_type,
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'app.log',
            'formatter': 'default'
        },
        'consolehandler': {
            'level': logging_type,
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        }
    },
    'loggers': {
        'app': {
            'handlers': [ 'consolehandler','rotatingfile'],
            'level': logging_type,
            'propogate': True,
        },
        'console': {
            'handlers': ['consolehandler'],
            'level': logging_type,
            'propogate': True
        }
    }
}

dictConfig(LOGGING_CONFIG)
logger = logging.getLogger('app')



# default_symbol = {"ES":{"maturityDate":20210917,"exchange":"GLOBEX"},"YM":{"maturityDate":20210917,"exchange":"ECBOT"},
#                   "NQ":{"maturityDate":20210917,"exchange":"GLOBEX"},"RTY":{"maturityDate":20210917,"exchange":"GLOBEX"}}

# default_symbol = {"RTY":{"maturityDate":20210917,"exchange":"GLOBEX"}}


timeframe = 60

tws_host='127.0.0.1'
tws_port= 7497
ib_obj={}
useRTH = True